
package arrayofobjects;

import com.modules.Employee;
import java.util.Scanner;

public class ArrayOfObjects {

    public static void main(String[] args) {
        System.out.println("Hello World");
        
        Employee e[] = new Employee[3];
        
        Scanner sc = new Scanner(System.in);
        
//        System.out.println("Enter Name " + (i+1));
//        System.out.println("Enter Age " + + (i+1));
//        System.out.println("Enter Salary " + + (i+1));
        
        e[0] = new Employee("SHailesh", 22, 10000);
        e[1] = new Employee("Daya", 21, 20000);
        e[2] = new Employee("Pooja", 20, 25000);
        
//        for (int i = 0; i < 3; i++) {
//            e[i] = new Employee(sc.nextLine(), sc.nextInt(), sc.nextInt());
//        }

        int total_salary = 0;
        int age = 0;

        for (int i = 0; i < 3; i++) {
            total_salary += e[i].getSalary();
            age += e[i].getAge();
            System.out.println(e[i]);
        }
        
        System.out.println("Average Salary = " + total_salary/e.length);
        System.out.println("Average Age = " + age/e.length);
        
        int sub[] = {10, 20, 30, 40, 50};
        
        for (int i : sub) {
            System.out.println("Sub Marks " + i );
        }
        
        for (Employee employee : e) { // enhanced for loop
            System.out.println(employee);
        }
        
    }
    
}
