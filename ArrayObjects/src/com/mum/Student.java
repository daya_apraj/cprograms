/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mum;

/**
 *
 * @author ICIT
 */
public class Student extends Person{
    int rollno;
    int marks;

    public Student(int rollno, int marks, String name, int age, String city) {
        super(name, age, city);
        this.rollno = rollno;
        this.marks = marks;
    }

    
    
    public void setRollno(int rollno) {
        this.rollno = rollno;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    public int getRollno() {
        return rollno;
    }

    public int getMarks() {
        return marks;
    }

    @Override
    public String toString() {
        
        return super.toString()+"Student{" + "rollno=" + rollno + ", marks=" + marks + '}';
    }
    
}
